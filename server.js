require("dotenv").config();
const express = require("express");
const mongoose = require("mongoose");
const db = process.env.DB_URI;
const app = express();
const items = require("./routes/items");
// const Item = require("./models/Item");

app.use(express.json());

mongoose
  .connect(db, { useNewUrlParser: true, useUnifiedTopology: true })
  .then(() => console.log("Connected to salt data base"))
  .catch((err) => console.log(err));

// app.get("/", (req, res) => {
//   console.log("hellooooooooo");
// });
app.use("/api/items", items);
// app.get("/api/items", (req, res) => {
//   console.log("hellooooooooo");
//   Item.find()
//     .sort({ date: -1 })
//     .then((items) => res.json(items));
// });

const port = process.env.PORT || 5000;

app.listen(port, () => console.log(`server is up and running on port ${port}`));
